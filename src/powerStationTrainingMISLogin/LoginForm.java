package powerStationTrainingMISLogin;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginForm {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private User user;
	private JLabel lblUserNameTip;
	private JLabel lblPSWTip;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm window = new LoginForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginForm() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("\u7535\u5382\u804C\u5DE5\u57F9\u8BAD\u4FE1\u606F\u7BA1\u7406\u7CFB\u7EDF");
		frame.setBounds(100, 100, 411, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u8D26\u53F7\uFF1A");
		label.setBounds(54, 50, 54, 15);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u5BC6\u7801\uFF1A");
		label_1.setBounds(54, 101, 54, 15);
		frame.getContentPane().add(label_1);
		
		textField = new JTextField();
		textField.setBounds(128, 47, 131, 30);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(128, 98, 131, 30);
		frame.getContentPane().add(passwordField);
		
		JButton button = new JButton("\u767B\u9646");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(validateUser()){
				//如果通过语法验证，则进行判断用户是否为合法用户
				user = new User(textField.getText(),new String(passwordField.getPassword()));
				if(user.findUser())
					System.out.println("登陆成功！");
				else 
					System.out.println("登陆失败！");
					
				
			}else
				System.out.println("用户没有通过！");
		}
	});
		button.setBounds(85, 156, 70, 23);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("\u53D6\u6D88");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		button_1.setBounds(189, 156, 70, 23);
		frame.getContentPane().add(button_1);
		
		lblPSWTip = new JLabel("");
		lblPSWTip.setBounds(269, 101, 98, 15);
		frame.getContentPane().add(lblPSWTip);
		
		lblUserNameTip = new JLabel("");
		lblUserNameTip.setBounds(269, 50, 98, 15);
		frame.getContentPane().add(lblUserNameTip);
	}
	private boolean  validateUser() {
		boolean result = true;
		
		if (textField.getText().isEmpty()) {
			this.lblUserNameTip.setText("请输入用户名！");
			result =result && false;
		}
		if(new String(passwordField.getPassword()).isEmpty()){
			this.lblPSWTip.setText("请输入密码!");
			result =  result && false;
		}
		return result;
	}
}
